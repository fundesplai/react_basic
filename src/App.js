import React from 'react';
import Taula from "./Taula";
import Calendari from "./Calendari";

import {Container, Row, Col} from 'reactstrap'

import { BrowserRouter, Link, Switch, Route } from "react-router-dom";

import "./App.css";
import Barra from './Barra';

const MOTOS = [
  {
    marca: "honda",
    model: "scoopy"
  },
  {
    marca: "honda",
    model: "cbr125"
  },
  {
    marca: "yamaha",
    model: "fzr1000"
  },
  {
    marca: "gilera",
    model: "nexus"
  },
  {
    marca: "aprilia",
    model: "mana"
  },
]


function Inici(){
  return <h3>Pàgina inici</h3>
}

function App() {

  return (
      <BrowserRouter>
        <Container>
          <Row>
            <Barra />
          </Row>
         
          <Switch>
            <Route exact path="/"  component={Inici} />
            <Route exact path="/motos" render={() => <Taula dades={MOTOS} />} />
            <Route path="/motos/:marca" render={(props) => <Taula marca={props.match.params.marca} dades={MOTOS} />} />
            <Route exact path="/calendari" render={() => <Calendari mes={4} any={2021} />} />
            <Route path="/calendari/:any/:mes" render={
              (props) => <Calendari 
                      mes={props.match.params.mes}
                      any={props.match.params.any} 
                    />} />
            <Route render={() => <h1>Not found</h1>} />
          </Switch>
        </Container>
      </BrowserRouter>
  );
}

export default App;
