import React from "react";
import { Table } from "reactstrap";

function Taula(props) {
  const motosFiltre = props.marca
    ? props.dades.filter((el) => el.marca === props.marca)
    : props.dades;

  const files = motosFiltre.map((moto) => {
    const fila = (
      <tr>
        <td>{moto.marca}</td>
        <td>{moto.model}</td>
      </tr>
    );
    return fila;
  });

  return (
    <>
      <br />
      <h4>Marca: {props.marca}</h4>
      <Table>
        <thead>
          <tr>
            <th>Marca</th>
            <th>Model</th>
          </tr>
        </thead>
        <tbody>{files}</tbody>
      </Table>
    </>
  );
}

export default Taula;
