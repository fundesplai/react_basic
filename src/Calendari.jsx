import React from "react";
import "./Calendari.css";

const DIES = ["Dl", "Dm", "Dc", "Dj", "Dv", "Ds", "Dg"];
const MESOS = [
  "Gener",
  "Febrer",
  "Març",
  "Abril",
  "Maig",
  "Juny",
  "Juliol",
  "Agost",
  "Setembre",
  "Octubre",
  "Novembre",
  "Desembre",
];

function Mes({ mes }) {
  return <h2>{MESOS[mes - 1]}</h2>;
}

function Dia({ valor, festiu }) {
  return <div style={{color: festiu ? "red" : "black"}} className={valor === 0 ? "dia amagat" : "dia"}>{valor}</div>;
}

// function Setmana({dies}) {
function Setmana(props) {
  return (
    <div className="setmana">
      {props.dies.map((el, idx) => (
        <Dia key={idx} festiu={idx===5||idx===6} valor={el} />
      ))}
    </div>
  );
}

function Calendari({ mes, any }) {
  const primerDia = new Date(any, mes - 1, 1);
  const ultimDia = new Date(any, mes, 0);
  const diesTotals = ultimDia.getDate();

  let diaSetmana = primerDia.getDay();
  //ull diasetmana volem que vagi de dilluns a diumenge, el dilluns que normalment és 1 ha de ser 0, el diumenge serà 6
  // ho deixarem tal com està, només que el diumenge el considerarem el dia 7
  if (diaSetmana === 0) diaSetmana = 7;

  let llistaDies = []; // contindrà tots els dies del mes
  //primer afegim els blancs, si dia setmana és 3 (dimecres) haurem d'afegir 2 zeros davant
  for (let index = 1; index < diaSetmana; index++) {
    llistaDies.push(0);
  }
  // ara els dies del mes
  for (let dia = 1; dia <= diesTotals; dia++) {
    llistaDies.push(dia);
  }
  // a llistaDies hi tenim 0,0,...,1,2,3...,30,31
  //calculem setmanes
  let numSetmanes = Math.ceil(llistaDies.length / 7);
  let setmanes = [];
  for (let index = 0; index < numSetmanes; index++) {
    let setmana = llistaDies.splice(0, 7);
    setmanes.push(<Setmana key={index} dies={setmana} />);
  }

  return (
    <>
      <Mes mes={mes} />
      <Setmana dies={DIES} />
      {setmanes}
    </>
  );
}

export default Calendari;
